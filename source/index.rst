.. rtfdtest documentation master file, created by
   sphinx-quickstart on Tue Apr 20 09:27:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. raw:: html

    <script type="text/javascript">
    if (String(window.location).indexOf("readthedocs") !== -1) {
        window.alert('The documentation has moved. I will redirect you to the new location.');
        window.location.replace('https://example.com');
    }
    </script>
Welcome to rtfdtest's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
